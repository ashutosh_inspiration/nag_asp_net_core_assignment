﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Web.MVC
{
    public static class Extensions
    {
        public async static Task<bool> IsAdminUser(this ControllerBase controller, UserManager<IdentityUser> _userManager)
        {
            if (controller.User != null && controller.User.Identity != null && !String.IsNullOrEmpty(controller.User.Identity.Name))
            {
                var user = await _userManager.FindByEmailAsync(controller.User.Identity.Name);
                return  await _userManager.IsInRoleAsync(user, "Admin");
            }
            return false;
        }
    }
}
