﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.MVC.Models
{
    public class Cart
    {
        public Cart()
        {

        }
        public Cart(string  userName)
        {
            this.Id = Guid.NewGuid();
            this.UserName = userName;
            this.Order = new Order();
        }
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public Order Order { get; set; }
    }
}
