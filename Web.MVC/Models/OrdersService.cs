﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Core.Dtos;

namespace Web.MVC.Models
{
    public class OrdersService : IOrdersService
    {
        public OrdersService()
        {
            this.CartItems = new List<Cart>();
            this.PlacedOrders = new List<Cart>();
        }
        public IList<Cart> CartItems { get; set; }
        public IList<Cart> PlacedOrders { get; set; }

        public void AddProductToCart(string userName, ProductDto product)
        {
            var cart = this.CartItems.FirstOrDefault(x => x.UserName == userName);
            if (cart != null)
            {
                cart.Order.Products.Add(product);
            }
            else
            {
                cart = new Cart(userName);
                cart.Order.Products.Add(product);
                this.CartItems.Add(cart);
            }
        }

        public int? GetCartItemsCount(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return null;
            }
            var cart = this.CartItems.FirstOrDefault(x => x.UserName == userName);
            if (cart != null)
            {
                return cart.Order.Products.Count;
            }
            else
            {
                this.CartItems.Add(new Cart(userName));
                return 0;
            }
        }

        public Cart GetCart(string userName)
        {
            var cart = this.CartItems.FirstOrDefault(x => x.UserName == userName);
            if (cart != null)
            {
                return cart;
            }
            else
            {
                this.CartItems.Add(new Cart(userName));
                cart = this.CartItems.FirstOrDefault(x => x.UserName == userName);
                return cart;
            }
        }

        public void PlaceOrder(string userName)
        {
            var cart = this.CartItems.First(x => x.UserName == userName);
            var placeOrder = new Cart()
            {
                Id = cart.Id,
                UserName = cart.UserName,
                Order = cart.Order
            };
            placeOrder.Order.Id = cart.Id;
            placeOrder.Order.PlacesOn = DateTime.Now;
            placeOrder.Order.IsPlaced = true;

            this.PlacedOrders.Add(placeOrder);
            this.CartItems.Remove(cart);
        }

        public IEnumerable<Order> GetOrderHistory(string userName)
        {
            return this.PlacedOrders.Where(x => x.UserName == userName).Select(x => x.Order);
        }

        public void RemoveItem(string userName, Guid id)
        {
            var cart = this.CartItems.First(x => x.UserName == userName);
            cart.Order.Products.Remove(cart.Order.Products.First(x => x.Id == id));
        }

        public Cart getOrder(string name, Guid id)
        {
            var res = this.PlacedOrders.FirstOrDefault(x => x.Id == id);
            return res;
        }
    }
}
