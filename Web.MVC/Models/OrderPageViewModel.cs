﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.MVC.Models
{
    public class OrderPageViewModel
    {
        public Cart Cart { get; set; }
        public IEnumerable<Order> PastOrders { get; set; }
    }
}
