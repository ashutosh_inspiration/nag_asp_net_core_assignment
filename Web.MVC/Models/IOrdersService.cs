﻿using System;
using System.Collections.Generic;
using Web.Core.Dtos;

namespace Web.MVC.Models
{
    public interface IOrdersService
    {
        IList<Cart> CartItems { get; set; }
        IList<Cart> PlacedOrders { get; set; }

        void AddProductToCart(string userName, ProductDto product);
        Cart GetCart(string userName);
        int? GetCartItemsCount(string userName);
        IEnumerable<Order> GetOrderHistory(string userName);
        void PlaceOrder(string username);
        void RemoveItem(string userName, Guid id);
        Cart getOrder(string name, Guid id);
    }
}