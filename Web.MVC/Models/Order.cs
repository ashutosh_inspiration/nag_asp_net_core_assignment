﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Core.Dtos;

namespace Web.MVC.Models
{
    public class Order
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Order()
        {
            Products = new List<ProductDto>();
        }
        public IList<ProductDto> Products { get; set; }
        public int TotalItems
        {
            get
            {
                return Products.Count();
            }
        }
        public double TotalPrice
        {
            get
            {
                return Products.Sum(x => x.SalesPrice);
            }
        }
        public bool IsPlaced { get; set; }

        public DateTime? PlacesOn { get; set; } = null;
    }
}
