﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Web.Core.Dtos;
using Web.Core.Models;
using Web.MVC.Models;

namespace Web.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public readonly UserManager<IdentityUser> _userManager;
        private readonly IMapper _mapper;
        private readonly IOrdersService _orderService;

        public HomeController(ILogger<HomeController> logger, UserManager<IdentityUser> _userManager, IMapper mapper, IOrdersService orderService)
        {
            _logger = logger;
            this._userManager = _userManager;
            _mapper = mapper;
            this._orderService = orderService;
        }

        public async Task<IActionResult> Index(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ViewData["Message"] = message;
            }
            var client = new HttpClient();
            var productResult = await client.GetAsync("http://localhost:5010/api/products/getall");

            ViewBag.CartItem = this.GetCartItemsCount();

            var products = JsonConvert.DeserializeObject<IEnumerable<Product>>(await productResult.Content.ReadAsStringAsync());

            var prod = _mapper.ProjectTo<ProductDto>(products.AsQueryable());

            return View(prod);
        }

        public async Task<IActionResult> PostIndex(string search, string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ViewData["Message"] = message;
            }

            var client = new HttpClient();
            ViewBag.Search = string.IsNullOrEmpty(search) ? "" : search;
            ViewBag.CartItem = this.GetCartItemsCount();
            var productResult = await client.GetAsync("http://localhost:5010/api/products/Search?name=" + search);

            var products = JsonConvert.DeserializeObject<IEnumerable<Product>>(await productResult.Content.ReadAsStringAsync());

            var prod = _mapper.ProjectTo<ProductDto>(products.AsQueryable());

            return View("Index", prod);
        }

        [HttpPost]
        public async Task<IActionResult> AddItemToCart(Guid productId, string search)
        {
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                var client = new HttpClient();

                var itemResult = await client.GetAsync("http://localhost:5010/api/products/" + productId);

                var item = _mapper.Map<ProductDto>(JsonConvert.DeserializeObject<Product>(await itemResult.Content.ReadAsStringAsync()));

                _orderService.AddProductToCart(User.Identity.Name, item);
            }
           
            if (string.IsNullOrEmpty(search))
            {
                return RedirectToAction("Index", new { message = string.IsNullOrEmpty(User.Identity.Name) ? "Please Login to add product into cart!" : "" });
            }
            else
            {
                return RedirectToAction("PostIndex", new { search = search, message = string.IsNullOrEmpty(User.Identity.Name) ? "Please Login to add product into cart!" : "" });
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private int? GetCartItemsCount()
        {
            return this._orderService.GetCartItemsCount(User.Identity.Name);
        }
    }
}
