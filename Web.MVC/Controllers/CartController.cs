﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.MVC.Models;

namespace Web.MVC.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private readonly IOrdersService ordersService;

        public OrderPageViewModel viewModel = new OrderPageViewModel();

        public CartController(IOrdersService _ordersService)
        {
            ordersService = _ordersService;
        }
        // GET: Order
        public IActionResult Index(string message)
        {
            ViewBag.Message = message;
            this.viewModel.Cart = ordersService.GetCart(User.Identity.Name);
            this.viewModel.PastOrders = ordersService.GetOrderHistory(User.Identity.Name);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult PlaceOrder()
        {
            ordersService.PlaceOrder(User.Identity.Name);
            return RedirectToAction("Index", new { message = "Order Placed Successfully!" });
        }

        public IActionResult Delete(Guid id)
        {
            this.ordersService.RemoveItem(User.Identity.Name, id);
            return RedirectToAction("Index",  new { message = "Item Removed!" });
        }
    }
}