﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.MVC.Models;

namespace Web.MVC.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IOrdersService ordersService;

        public OrderPageViewModel viewModel = new OrderPageViewModel();

        public OrderController(IOrdersService _ordersService)
        {
            ordersService = _ordersService;
        }
        // GET: Order
        public IActionResult Index()
        {
            this.viewModel.PastOrders = ordersService.GetOrderHistory(User.Identity.Name);
            return View("List", viewModel);
        }

        public IActionResult Details(Guid  id)
        {
            this.viewModel.Cart = ordersService.getOrder(User.Identity.Name, id);
            return View(this.viewModel);
        }
    }
}