﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Core.Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public double Price { get; set; }
        public double SalesPrice
        {
            get
            {
                return Math.Round(Price * 1.1, 2);
            }
        }

    }
}
