﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Core.Dtos
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public double SalesPrice { get; set; }
    }
}
